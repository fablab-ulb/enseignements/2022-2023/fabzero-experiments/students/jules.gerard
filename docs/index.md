Yo! Bienvenue sur ma page du du cours de FabZero Experiments. :sunglasses: 

## [Qui suis-je ?](https://www.youtube.com/watch?v=2soGJXQAQec&ab_channel=SnoopDoggVEVO)

Je me nomme Jules, Rulio ou Rouleau. Je suis en vie sur Terre depuis 23 années, j'ai grandi pas loin d'ici, vers la sortie 4  de la E411 plus précisemment. Et je suis ce cours en étant au niveau trois  d'un bachelier de biologie à l'ULB. 

![](images/lol.jpg)

## [Ma life](https://www.youtube.com/watch?v=ibPslj6BMcI&ab_channel=OxmoPuccino%5BOfficiel%5D)

La principale activité et passion de ma vie est le rugby, sinon je kiffe voyager et voir  de beaux couchers de soleil,  j'aime bien prendre des photos, customiser des objets, je prends beaucoup de plaisir à faire bouger mon corps en équilibre sur tous types de planches qu'elles aient des roulettes ou pas, j'apprécie festoyer en bar comme en cave, et sinon j'adore me promener chez Cashconverter ou chez Brico.

![](images/IMG_20201025_0034.jpg)


## [Skielz](https://www.youtube.com/watch?v=4JpYWGA2BUI&ab_channel=Peet-Topic)

En termes de choses concrètes ou de projets dans lesquels j'ai pris part : 
* Stage de permaculture chez éconnaissances, ce qui a donné suite à chaque année un beau potager chez mes parents. 
* Deux semaines de volontourisme (faut pas se mentir) à Camaronal au Costa Rica ayant comme objectif principal la protection de tortues de mers. 
* Participation à un chantier de 3 semaines dans l'évenementiel au CHIO de Aachen.
* Construction d'une mini-rampe de skate dans le fond de mon jardin pendant le premier confinement. 
