# 1. Le commencement du début 

Lors de ce premier cours nous avons découvert le **FabLab de l'ULB**, ainsi que tous ses locaux et ateliers avec plein de belles machines qu'on apprendra à utiliser par la suite. Le but de ce module est de nous initier aux commandes UNIX en utilisant un terminal, mais aussi avoir des bases de project management, tout ça en documentant notre apprentissage et en le partageant sur [**GitLab**](https://gitlab.com/).



## 1.1 Command line interface (CLI)

La plupart des programmes que nous utilisons sur notre ordinateur se manipule à l'aide d'un **Graphical User Interface (GUI)** mais il est bien plus intéressant de savoir utiliser ceux-ci avec un **Command line interface (CLI)** ou *terminal*. L'environnement qui est utilisé est un *Shell*, celui qu'on utilise dans ce cas ci est **Bash** (Bourne Again Shell). J'ai appris à utiliser cet outil via [ce tutoriel](https://github.com/Academany/fabzero/blob/master/program/basic/commandline.md) du FabZero sur GitHub.



### 1.1.1 Navigation à travers le système de fichiers 

La navigation se fait à l'aide de *command lines* qui est la combinaison d'une commande, d'une option et d'un argument. Voici un exemple : 

```
  ls -a /usr/local/bin
```
>*ls* est la commande et liste les fichiers et dossiers, *-a* est l'option qui permet de montrer de potentiels fichiers cachés et */usr/local/bin* est l'argument qui indique où appliquer la commande. 

Voici une liste de commandes basiques à savoir utiliser : 

| Commands    | Description                      |
|-------------|----------------------------------|
| ls          | List files and folders           |
| pwd         | Print working directory          |
| mkdir       | Create a directory               |
| cd          | Change directory                 |
| cp          | Copy files and folders           | 
| mv          | Move files and folders           |   
| rm          | Remove files and folders         | 
| find        | Find files and folders           | 



## 1.2 Documentation 

**Documenter son travail** permet de mieux le comprendre et de garder une trace de chaque étape qui a été franchie. La philosphie du *"c'est pas la destination qui compte mais le voyage pour y arriver"* démontre bien l'idée de la chose, ce n'est pas non plus la description d'un tutoriel mais plutôt de raconter son histoire et la façon dont on l'a vécue. Le partager sur notre page web (en fichier HTML) est très important aussi car cela permet de donner accès à n'importe qui dans le monde qui en aurait besoin. Mon apprentissage a commencé par suivre [ce tutoriel FabZero](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md#writing-documentation-in-markdown).

Tout d'abord j'ai du télécharger un éditeur de texte, j'ai opté pour [Visual Code Studio](https://code.visualstudio.com/) comme conseillé dans le tuto. 

Le format **Markdown** est celui qu'on utilise car il est plus pratique à utiliser que le HTML. C'est un langage facile à écrire, facile à lire et facilement exportable en HTML par la suite. Nos fichiers sont sous format `.md` et sont transformés en HTML grâce à un autre fichier qui se trouve dans le dossier sur notre compte GitHub dont je parlerai par la suite. J'ai appris l'utilisation du **Markdown** via [ce rapide tutoriel ](https://www.markdowntutorial.com/). 



### 1.2.1 Images 

Il est inutile d'uploader des images trop volumineuses ou de qualité 4K sur notre site. Ayant déjà de l'expérience dans la photo j'avais sur mon ordinateur le **logiciel opensource** [Darktable](https://www.darktable.org/) que j'ai utilisé pour diminuer la qualité et exporter en changeant la taille de l'image (nombre de pixels). 

![Exemple de l'interface **Darktable**](images/Darktable2_01.jpg)

![Outil export](images/Darktable1.jpg)

Nous avons aussi appris à utiliser [GraphicsMagick](http://www.graphicsmagick.org/) afin de convertir des images `.png` en `.jpg`. Une fois le logiciel téléchargé, on utilise des commandes pour convertir nos fichiers : 

![Conversion png en jpg :](images/commande_gm.jpg) 



### 1.2.2 Git 

Il est possible de travailler sur notre site web [GitLab](https://gitlab.com/) depuis notre ordinateur *"en local"*, cela permet de ne pas nécessiter une connexion internet pour travailler dessus. En téléchargeant [Git](https://git-scm.com/) cela nous donne accès à **GitBash** qui est le terminal reliant le site web à mon ordinateur. 

![Vérification de la version et connexion à mon compte GitLab](images/gitcheck.jpg)



#### 1.2.2.1 Génération clé SSH

J'ai suivi [ce tutoriel GitLab](https://docs.gitlab.com/ee/user/ssh.html) qui m'a appris à généré une clé SSH à l'aide de *cette commande* :
```
ssh-keygen -t ed25519 -C "<comment>"
```
Il existe plusieurs types de clé SSH mais je ne me suis pas cassé la tête et j'ai généré une clé "ED25519".

Ensuite j'ai du choisir l'emplacement de celle-ci : 
```
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/jules/.ssh/id_ed25519): 
```
Et enfin j'ai du écrire un mot de passe pour sécuriser le tout.
```
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
```


#### 1.2.2.2 Ajout de la clé SSH à mon compte GitLab

Une fois la clé SSH généré, il a fallu lié cette clé à mon compte GitLab. Pour ce faire j'ai du encoder le contenu (sous format texte) du fichier .pub (SSH publique) qui a été généré, dans l'onglet "clé SSH" des préférences de mon compte GitLab : 

![Clé SSH reliée](images/sshgitlab.jpg)

Ensuite il faut cloner le repository qui a été créé sur mon ordinateur. Cela se fait en copiant l'URL de la clé reliée à mon compte GitLab : 

![Clonage avec SSH](images/clonessh.jpg)

Voici la commande pour vérifier que tout fonctionne correctement : 

![](images/testgit.jpg)

Maintenant que ma connexion est sécurisée, je peux travailler depuis mon ordinateur sur la copie du projet. Il y a quelques commandes à connaitre pour permettre de mettre à jour ou récupérer la dernière version de mon projet en ligne : 

```
git pull 
```

Permet de télécharger la dernière version du projet. Une fois que l'on a fait ce qu'on avait à faire dans les fichiers, on utilise ces commandes pour remettre en ligne la version mise à jour : 

```
git add -A
```
```
git commit -m "message d'explication de la maj"
```
```
git push
```


## 1.3 Principes de project management 

Nous avons aussi été initié à certains principes de gestion de projet afin de mener au mieux nos futurs projets et surtout dans le délais imparti. Voici les différentes techniques dont on nous a parlé : 

- **As-You-Work Documentation** qui est tout simplement ce que je suis entrain de faire sur ce site. Ça consiste à documenter, écrire, expliquer un travail dès le début d'un projet. Ce n'est pas simplement décrire les étapes bêtement sous forme de tiret mais plutôt d'expliquer la façon de découvrir et d'aborder un problème ou une nouvelle compétence à apprendre. Ça va nous permettre de garder une trace de notre apprentissage, de mieux se rappeler de chaque étape mais aussi de pouvoir le partager à quiconque aimerait s'y intéresser par la suite. Je vous redirige vers [cette page](http://fablabkamakura.fabcloud.io/FabAcademy/support-documents/projMgmt/#modular-hierarchical-planning) documentée par une personne travaillant dans un Fablab au Japon et qui explique tous ces fameux principes de project management accompagnés de beaux schémas. 

Même si cette citation peut paraître cul-cul, elle illustre bien l'idée de ce principe : *"Le plus important, ce n’est pas la destination, mais les mésaventures et les souvenirs que l’on crée le long du chemin." - Penelope Riley*

- **Modular & Hierarchical Planning** est une façon de voir les choses face à un défi ou projet de grande taille qui consiste à le séparer en plusieurs morceaux/tâches plus simples à réaliser et à ensuite tout assembler dans un ordre précis et logique. 

- **Demand vs Supply - side Time Management** est dans le même ordre d'idée mais concerne le temps et est pour moi **la technique la plus importante**. C'est une méthode d'organisation qui consiste à diviser un travail en temps et non en tâches. Il faut d'abord décider combien de temps on veut consacrer à ce travail durant par exemple une semaine, fragmenter ce temps en plusieurs "sessions" de travail, puis de faire une liste de toutes les tâches qu'on a à faire et anticiper combien de temps elles nous prendrons chacunes. Il """"suffit"""" ensuite de dispatcher ces tâches dans nos créneaux adéquats, et surtout de s'y tenir!

![](images/SSTM.jpg)


- **Spiral development** est une façon de perfectionner son projet. Elle consiste à commencer par faire une sorte de brouillon (déjà plutôt abouti) d'un projet et puis par étapes successives de rajouter des versions de plus en plus affinées et perfectionnées. 

- **Triage** est une vision des choses qui vise à prioriser les tâches les plus importantes par rapport à celles moins importantes. 