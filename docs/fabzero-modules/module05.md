# 5. Dynamique de groupe et projet final

Ce module a comme objectif de nous apprendre à faire le bon acheminement de réflexion quant à une problématique pour notre projet de groupe final. 



## 5.1 Problem and solution tree 

Nous avons été introduit à une technique qui s'appelle **"l'arbre à problèmes et à solutions"**. Cette technique est faite pour nous aider à mieux comprendre une problématique, ses causes et ses effets. 

Pour utiliser cette technique le mieux possible, il faut imaginer ou schématiser (par ordinateur ou en dessin) **deux arbres** avec des **branches** et des **racines**. Ensuite il faut identifier un problème principal, qui sera représenté par le tronc d'un **premier arbre**. Les racines de ce problèmes seront les causes principales qui l'ont généré. Les branches représenteront ce que les racines et le tronc auront concrètement causés dans notre société ou environnement. 

Pour le **deuxième arbre**, le tronc est maintenant représenté par le résultat que l'ont veut obtenir. Les racines sont les chemins qu'il faudra prendre pour arriver à cette objectif, les choses qu'il faudra mettre en place pour y parvenir. Et les branches représenteront les impacts que ce changement pourrait avoir sur notre société ou sur l'environnement. A chaque racine ou branche, il est recommandé de pousser la réflexion plus loin en ramifications, en effets secondaires ou en causes indirectes etc. 

Cette façon de faire est parfaitement adaptée au travail de groupe car chacun peut y mettre du sien et participer à son échelle au commencement d'un projet. Et comme on dit dans le coin : **l'union fait la force!**. 



### 5.1.1 Exemple appliqué 

Voici ma construction de deux arbres à problèmes et à solutions en analysant le projet [**"L'énergie dans l'habitat"**](https://wiki.lowtechlab.org/wiki/L%27%C3%A9nergie_dans_l%27habitat) du *Low-tech Lab. Je me suis aidé de [ce template](https://app.mural.co/template/a9eb7d4a-8dad-4815-81af-53707f212cab/0553f1eb-8907-4862-bbbd-1a5db28e2bb3) sur le site de [Mural](https://www.mural.co/).

Arbre à problèmes : 


![](images/arbre_problem.jpg)


Arbre à solutions : 


![](images/arbre_objectif.jpg)



## 5.2 Formation des groupes et émergence de problématiques (07/03/23)

Pour nous initier au projet final, nous nous sommes retrouvés dans une grande pièce avec beaucoup d'espace et la consigne était d'amener un objet avec soi qui représenterait un thème ou une problématique qui nous intéresse. J'ai pris avec moi une **gourde** car je voulais que ça représente un problème majeur dans le monde : **l'accessibilité à l'eau potable**. 

**Ébauche de groupe** 

Nous avons d'abord chacun du poser notre objet au milieu de la pièce, en essayant de les espacer les uns par rapport aux autres et nous nous sommes tous mis en cercle autour de ceux-ci afin de tous pouvoir les voir. Ensuite chacun de nous avons récupéré notre objet et avons du se regrouper en fonction de ceux des autres et du lien qu'il pourrait y avoir entre nos objets. En discutant avec un peu tout le monde je me suis vite retrouvé avec 3 étudiants ayant aussi des gourdes avec eux.

Voilà donc notre groupe formé, voici ma team de choc :

- [Jonathan](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/jonathan.kahan/)
- [Gilles](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/gilles.theunissen/)
- [Thomas](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/thomas.marechal/()
- [Lucie](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/Lucie.lestienne/)


**Trouver un thème** 

Devant nous se trouvait 4 marqueurs de couleurs différentes ainsi qu'une feuille blanche A3, la consigne qui nous a été donné était de noter un maximum de mot en lien avec l'objet qu'on a amené. Ensuite sur un post-it à nouveau chacun d'une couleur différente nous avons du noter un thème plus précis de l'idée de base qu'on avait en arrivant ici, chacun en lien avec notre thématique. Voilà ce que ça a donné : 

![](images/brainsto.jpg)


Jonhathan et moi avions une gourde pour illustrer le même problème, Gilles l'avait pour illustrer la solution aux bouteilles en plastique, Thomas l'avait pour la même solution mais plus dans un objectif de revoir la chaîne de production, ce jour là Lucie elle était représentée par un gobelet jetable et dont le problème illustré parlait du grand nombre de gobelets à usage unique jeté chaque jour. 

Ensuite sur une autre feuille nous devions nous positionner par rapport à la **problématique** de chacun, nous étions donc un peu séparé en deux. Deux d'entre nous voulions plus se diriger vers l'accessibilité et la qualité de l'eau tandis que l'autre moitié était plus dans une optique de **repenser la production d'un objet (non recyclable) de tous les jours**. C'est vers cette deuxième problématique que nous nous sommes tous mis d'accord car cela nous parraissait plus facilement réalisable avec les outils du Fablab. 

Une fois la problématique trouvée, nous devions faire un exercice sous une forme spéciale, avec comme thème notre problématique. Il consistait à prendre la parole en commençant par "à ta place je ... " (par exemple : *à ta place je remplacerais ta bouteille d'eau par une gourde*), et ainsi faire une liste d'idées d'objets de tous jours qu'on pourrait remplacer. Après cet exercice un de nous a du aller faire le tour des autres groupes en exposant notre problématique et en recueuillant un maximum d'éléments liés à celle-ci. Nous avons donc aussi aidé d'autres groupes en répondant à leurs problématiques. Voici notre résultat : 

![](images/brainsto_liste.jpg)



## 5.3 Dynamique de groupe (28/03/23)

Lors de cette séance nous avons appris différents outils pour arriver à fonctionner au mieux dans un groupe. Les outils que je vais décrire sont des techniques de communication, de prises de décisions ou encore des façons pour être un maximum efficace lors de réunions. Voici le tableau récapitulatif de la séance : 

![](images/dyn_groupe.jpg)


### 5.3.1 Prises de décisions : le consensus 

Le mode de prise de décision par consensus est un processus qui vise à parvenir à un accord ou à une décision acceptée par tous les membres d'un groupe. Contrairement à d'autres méthodes de prise de décision, telles que la majorité ou la prise de décision par un leader, le consensus se caractérise par une approche plus collaborative et participative. J'ai choisi ce décrire cet outil de prise de décision car c'est comme ça que nous avons procédés lors de nos réunions. Je trouve que c'est la meilleure façon de fonctionner car l'objectif du consensus est d'obtenir une adhésion maximum de tous les membres impliqués, en cherchant à résoudre les désaccords et à trouver des solutions qui répondent aux préoccupations de chacun. Ça renforce la confiance du groupe et encourage la responsabilité partagée.

### 5.3.2 Rôles de réunion 

Les rôles de réunion sont des fonctions attribuées aux membres d'un groupe lors d'une réunion ou d'un projet de groupe. Ils contribuent à la gestion efficace de la réunion, à l'organisation des travaux et à la participation de tous les membres. Voici quelques exemples de rôles de réunion que nous avons pu essayés lors de nos entrevues :

1. Animateur : L'animateur est responsable de la gestion globale de la réunion. Ses responsabilités comprennent la facilitation des discussions, la garantie de la participation équitable des membres et la prise de décisions. L'animateur veille à maintenir un environnement respectueux, encourageant la créativité et la collaboration, ce qui favorise l'efficacité de la réunion.
2. Secrétaire : Le secrétaire est chargé de prendre des notes de tout ce qui est dit lors de la réunion. Le rôle de secrétaire garantit la capture des points clés, des décisions prises, ou encore la documentation des avancements et des résultats.
3. Gestionnaire du temps (*le temps c'est l'argent*): Le gestionnaire du temps s'assure que la réunion reste dans les limites de temps définies. Il surveille l'avancement des discussions, facilite la répartition équitable du temps entre les différents sujets et rappelle les membres lorsque le temps imparti pour un sujet est écoulé. La gestion efficace du temps aide à maintenir la productivité de la réunion et à s'assurer que tous les points importants sont abordés.

Il en existe d'autres mais ceux-ci sont pour moi les plus importants, je pense qu'il est judicieux de trouver un rôle à chaque personne du groupe si possible afin que personne ne se sente à l'écart. 

### 5.3.3 Météo d'entrée et de sortie 

À chaque début et fin de réunion, il est utile de faire un tour de météo afin de favoriser un climat positif et de faciliter l'ouverture ou fermeture de la communication. 

En début de réunion : pour savoir de quel humeur chaque personne est en fonction du déroulement de sa journée. Mais aussi quelles sont les attentes ou les préoccupations de chacun avant d'entamner les discussions. 

En fin de réunion : vise à recceuillir les impressions de tout le monde. Chacun partage son ressenti sur la réunion et si il faut discute de certains aspects à améliorer pour la prochaine fois. La météo de sortie aussi un bon moyen d'officialiser la fin de la réunion et de manière constructive!
