# 3. Impression 3D

Ce module va nous initier à *l'impression 3D* en nous apprenant à identifier les avantages et limitations de cette pratique. Nous aurons aussi l'occasion d'imprimer nos modélisations sur OpenSCAD afin de les tester IRL!



## 3.1 Logiciel de découpage en tranches  

[Ce tutoriel du FabLab](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md) va nous permettre de nous initier à tout ça. [PrusaSlicer](https://help.prusa3d.com/article/install-prusaslicer_1903) est le logiciel qu'on utilisera pour imprimer nos pièces. Les machines du Fablab sont des *Prusa i3 MK3/S/S+* dont voici les spécifications : 
- Surface d’impression (XY) : *21cm x 21cm*
- Hauteur d’impression maximale (Z): *25cm*
- Diamètre de buse (au FabLab ULB) : *0,4mm*

Les formats de fichiers reconnus par le slicer sont *.stl* et *.obj*. 



## 3.2 Imprimer en 3D step by step


**Préparation du G-code**

Première étape : **Exportation du fichier** sur OpenSCAD. 


![](images/export_stl.jpg)


Une fois le fichier exporté, il faut l'importer sur **PrusaSclicer** soit en le glissant dans le logiciel soit en clickant sur le bouton **Ajouter** : 

![](images/plusa_ajout.jpg)

Il est conseillé d'orienter le modèle de sorte que la surface la plus grande soit en contact avec le plateau afin de maximiser l'équilibre de la pièce lors de l'impression. Ceci peut se faire en cliquant sur la figure ci-dessous. 


![](images/plusa_posi.jpg)


La **Sélection de l'imprimante** se fait en fonction du modèle de l'imprimante qui est à vérifier sur la machine elle-même, à l'arrière de celle-ci sur une étiquette avec une QR code. Il faut ensuite sélectionner le modèle dans la liste *Imprimante* :


![](images/plusa_imp.jpg)


Si le modèle n'est pas dans la liste, il vous faut l'ajouter en choississant le bon modèle et en cochant la case **0.4mm buse**. 


**Sélection du filament** : 

L'idéal est de savoir quel type de filament est celui dans la bobine que vous allez utiliser, il faut aller dans la liste filament et le sélectionner. Vous pouvez avoir accès au catalogue de filaments qui ont déjà un pré-réglage en cliquant sur l'option *Ajouter/Enlever des filaments*. Le filament de base *Generic PLA* peut s'utiliser avec ceux fait en PLA (Polylactic acid) et fonctionnera par défaut. 


![](images/plusa_filament.jpg)


Pour plus d'informations sur tous les différentes types de fils, [cette page peut vous aider](https://blog.prusa3d.com/advanced-filament-guide_39718/). 

Il faut faire attention à ne pas utiliser les filaments à base d'ABS, d'ASA ou de PS (sauf dans des conditions sécurisées) car ils dégagent des gazs nocifs. 

La **hauteur des couches** est aussi importante. Par défaut elle est sélectionnée à 0,2mm, c'est un bon compromis entre vitesse et qualité d'impression. Si vous désirez plus de détails, il est conseillé de réduire cette taille de couche. Inversemment, si vous cherchez à imprimer rapidement ou si la pièce est plutôt volumineuse, sélectionnez un millimétrage plus élevé. 


![](images/plusa_couche.jpg)


**Supports**

Si dans notre pièce à imprimer il y a une partie qui est **supérieure à 45° par rapport à la verticale**, la pièce risquerait de perdre l'équilibre lors de l'impression. C'est pourquoi il est possible de rajouter un **support** pour permettre d'éviter que la pièce ne tombe.

Il en existe de plusieurs types et sont modifiables dans l'onglet *Réglages d'impression*, *Supports*. 


![](images/plusa_supp.jpg)


Info utile à savoir, en haut à droite se trouve ces trois options : 


![](images/plusa_sae.jpg)


En fonction de votre pièce et de vos connaissances d'impressions 3D, vous pouvez clicker sur **Avancé** ou même **expert** et ainsi avoir une panoplies d'options en plus. Ceci s'applique pour beaucoup de paramètres du logiciel. 


**Remplissage** 

Le remplissage de la pièce est très important car c'est la fondation de celle-ci et jouera beaucoup sur sa solidité. La densité est par défaut réglée sur 15%, ce qui est un bon compromis entre vitesse d'impression et solidité. Il faut éviter d'aller en dessous de 10% car les couches horizontales ne tiendraient pas, et il est inutile d'aller au dessus de 35% car cela ne pourra plus améliorer la solidité.

Pour accéder aux modifications de remplissage il faut aller dans le même onglet que pour les supports, puis *Remplissage*. Le motif de base est celui du **Gyroïde** qui est un bon compromis des contraintes d'impression. 


**Jupe et bordure**

La jupe est un test avant le début de l'impression, il faut observer si l'adhérence au plateau est pareille partout.

La bordure sert à augmenter l'adhérence au plateau, ceci est très utile pour les pièces hautes et fines. A ne pas confondre avec le support qui lui est vraiment une sorte de pont entre la plaque et les pièces "en l'air". 

Tous ces réglages sont à nouveau dans l'onglet *Réglages d'impressions* puis *Jupe et bordure*, vous pouvez la désactiver ou jouer avec la *distance de l'objet* de la jupe pour pas que la bordure ne dépasse sur celle-ci. 


**Epaisseur des parois**

Une fois de plus pour jouer sur la solidité de la pièce, il est possible de modifier l'épaisseur des parois. Par défaut il est sur 2 couches, on peut en mettre 3 pour des parois plus solides. Attention à ce que les couches internes et externes ne se touchent pas à l'intérieur de la pièce car cela poserait des problèmes de superposition de couches. 

Pour plus d'informations sur tous les types de réglages possible : [voir site Prusa](https://help.prusa3d.com/fr/category/reglages-dimpression_212).


**Vérification et export du G-code**

Une fois que tous vos réglages sont bons, il faut cliquer sur le bouton **Découper maintenant** en bas à doite de l'onglet *Plateau*. Vous allez alors avoir une estimation du temps d'impression, s'il est trop long vous pourrez encore modifier certains réglages afin de la raccourcir. 

A ce stade ci, vous pouvez aussi vérifier le détail de l'impression ou les couches internes en faisant glisser le curseur orange à droite de la pièce. Vous pouvez maintenant **Exporter le G-code** en bas à droite, qu'il faut **enregistrer sur une carte sd** dans un fichier à votre nom pour pouvoir s'y retrouver facilement. 



## 3.3 Problèmes courants 

Attention à la potentielle **mauvaise adhérence de l'impression au plateau**, pour éviter cela il faut vérifier que le plateau soit bien nettoyé et qu'il n'y ait pas de restes de plastiques d'impressions précédentes. S'il faut, ajoutez une **bordure**. Et si il faut changer le filament, bien penser à calibrer la hauteur de la 1ère couche. Pour faire ça de la bonne manière : [voir cette page](https://help.prusa3d.com/fr/article/calibration-de-la-premiere-couche-i3_112364).

Il est aussi courant qu'il y ait des **noeuds dans la bobine de filament**, qui peut bloquer le filament lors de l'impression. Il faut bien faire attention lors du changement du filament, [voir tuto gitlab](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md).

La chaîne de [CNC Kitchen](https://www.youtube.com/@CNCKitchen) nous a été conseillée pour voir toutes les possibilités de l'impression 3D et c'est très impressionnant!