# 2. Conception Assistée par Ordinateur (CAO)

Le module de cette semaine porte sur la modélisation et la confection d'un kit de construction paramétrique en s'inspirant de [Flexlinks](https://www.compliantmechanisms.byu.edu/flexlinks). 



## 2.1 Compliant mechanisms & flexures

Ce type de mécanisme est composé d'un assemblage simpliste de structures liant des articulations élastiques à d'autres solides, permettant d'apporter une certaine flexibilité à la structure. Pour bien comprendre le principe, je vous conseille d'aller voir [cette page sur GitLab avec pas mal d'exemples concrets](https://gitlab.com/fablab-ulb/enseignements/fabzero/compliant-mechanisms-and-flexures).



## 2.2 Logiciels 2D

Il existe 2 catégories de logiciels 2D, ils se distinquent par le fait qu'ils fonctionnent soit avec des images matricielles, soit avec des images vectorielles. Le premier type de logiciel fonctionne avec des **pixels** tandis que l'autres avec des **vecteurs**. Les pixels sont plus simples à travailler, ils sont assemblés en points de couleur de forme carrée mais leur utilisation est limitée par une certaine résolution. Tandis que les vecteurs eux peuvent être visualisés et modifiés à n'importe quelle échelle. 

[Inkscape](https://inkscape.org/fr/) est un exemple de logiciel opensource qui permet de faire du dessin en deux dimensions. 



## 2.3 Logiciels 3D 

La même chose s'applique en trois dimensions, les images sont soit en pixels soit en vecteurs. 

Quelques logiciels nous ont été présentés, tel que : 

- [Antimony](https://github.com/mkeeter/antimony)
- [Fusion360](https://www.autodesk.com/products/fusion-360/overview?term=1-YEAR&tab=subscription)
- [Grasshopper](https://www.grasshopper3d.com/)
- [Onshape](https://www.onshape.com/products/education)

Ceux-ci sont tous des logiciels **privés/payants**, personnelement je n'ai pas été confronté à les utiliser dans le cardre de mes études mais certains étudiants pourraient y avoir accès gratuitement via l'université mais seraient contraints à ne plus pouvoir avoir d'accès à ceux-ci ni à leurx travaux après leurs études, d'où l'énorme avantage de travailler avec des logiciels **opensource** comme [OpenSCAD](https://openscad.org/) ou [FreeCAD](https://www.freecad.org/). 



## 2.4 OpenSCAD 

OpenSCAD est un logiciel permettant de faire de la modélisation 3D à l'aide de lignes de code. Grâce à des fonctions prédéfinies chacun peut faire parler sa créativité afin de créer un objet quelconque. 



### 2.4.1 Fonctions basiques

Grâce au [cheatsheet](https://openscad.org/cheatsheet/) nous pouvons modéliser toutes sortes de choses sur le repère orthonormé du logiciel. Les unités du logiciel sont en mm. Ce [tutoriel sur GitLab](https://gitlab.com/fablab-ulb/enseignements/fabzero/cad/-/blob/main/OpenSCAD.md) m'a permis de découvrir le logiciel.

Coder un simple cube de (largeur, hauteur, profondeur) : 

```
cube(10); 
```

![](images/cube.jpg)

Coder un parallélipipède rectangle (= "cube"), avec le même type de variables : 
```
cube([10,20,5]);
```

![](images/cube_rectangle.jpg)

Coder un cylindre de (hauteur, rayon inférieur, rayon supérieur) : 
```
cylinder(20, 10, 10);
```

![](images/cylindre.jpg)

Une fonction importante à connaitre est `$fn`, celle ci permet de définir la fragmentation de la forme. Au plus il est élevé, au plus la fragmentation sera élevée : 
```
$fn=100;
cylinder(20, 10, 10);
```

![](images/cylindre_fn.jpg)

Lorsque l'on travaille sur un objet qui va devoir s'assembler avec un autre ou qui doit tout simplement être réajusté au fur et à mesure du temps, il est toujours très utile de le rendre **"paramétrisable"**. Cela signifie qu'en amont de la fonction qui créer la forme, on assigne un nom de variable qu'on va ensuite mettre en tant que paramètre de variable dans le code de la forme. Une fois que le code se complexifie, on pourra changer qu'un seul paramètre et s'il est bien relié au reste, l'objet restera proportionnel et sera facilement modulable.



### 2.4.2 Opérations booléennes 

Certaines fonctions permettent d'unir, creuser ou sommer des objets :

```
$fn=100;
side= 10;
union(){
  cube(side,center=true);
  sphere(r=side/sqrt(2));
}
```

![](images/union.jpg)

```
$fn=100;
hull(){
    side=10;
    cube(side, center=true);
    sphere(r=side/sqrt(2));
}
```

![Fusion d'objets](images/hull.jpg)

La fonction `center=true` centre l'objet au milieu des axes. 


### 2.4.4 Transformations 

Voici différentes transformations utiles sur le logiciel : 

**Rotation d'un cube**

```
rotate([45,45,0])
cube(10);
```

![rotation d'un cube](images/cube_rota.jpg)

**Translation d'un cube**

```
translate([0,0,10])cube(10);
```

![translation d'un cube](images/cube_transla.jpg)

On peut combiner ces transformations du moment qu'on termine par un ";" avant la fonction cube(). 

```
rotate([45,45,0])
translate([0,0,10])
    cube(10);
```

![](images/cube_combi.jpg)



## 2.5 Kit Flexlinks

En collaboration avec [Martin Loumeau](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/jean-francois.loumeau/fabzero-modules/module02/) nous nous sommes lancé le défi de créer un "jumpstick" miniature à l'aide d'un compliant mechanism. 

Nous avons tenter de rendre notre code le plus paramétrique possible, la pièce a été entièrement faite avec seulement 3 paramètres et nous les avons séparés par module. 

Voici les paramètres généraux du jumpstick : ("//" permet de faire des commentaires dans le code afin de mieux le comprendre)
```
$fn=100;
side=5; // unité paramétrique
r_trous = side/3; // rayons trous
r_sphere = side/2; // rayons sphere
```


#### Module barre verticale

```
module barre_v(){ // barre verticale
  difference(){
    cube([side/sqrt(2),side/sqrt(2),side*20],center=true);
      rotate([90,0,0])
      translate([0,side*9.9,0]) //permet au cylindre de se clipser
        cylinder(side*6,side/3.9,side/3.9,center = true);
  }
}
```

![](images/barre_v.jpg)



#### Module barre horizontale (bas du compliant mechanism) 

```
module barre_hd(){ // barre horizontale dessous
  translate([0,0,-side*3])
    difference(){
        rotate([90,0,90])
        translate([0,-side*5,0])
          cube([side*3,side,side],center=true);
            //trou 1 droite
        rotate([90,0,90])
        translate([side,-side*5,0])
          cylinder(side*2,r_trous,side/3,center=true);
            //trou 1 gauche
        rotate([90,0,90])
        translate([-side,-side*5,0])
          cylinder(side*2,r_trous,side/3,center=true);
    }
}
```

![](images/barre_hd.jpg)



#### Module shpère au contact du sol 

Cette pièce sert à réduire un maximum le contact avec le sol afin de pouvoir se diriger lors des rebonds. 

```
module sphere_sol(){ // contact sphere sol
    translate([0,0,-side])
    difference(){
      translate([0,0,-side*9])
        sphere(r_sphere);
    }   
}
```

![](images/sphere.jpg)

Voici le résultat de l'assemblage des 3 premiers modules, elle sera imprimée comme cela.

![](images/piece1.jpg)



#### Module poignées 

Cylindre creux :
```
module cylindre_p (){ // cylindre poignées

rotate([90,0,0])
translate([0,side*9.5,0])
    difference(){
      cylinder(side*6,side/4,side/4,center = true);
      cylinder(side*6,side/5,side/5,center = true);
    }
}
```

Centrage de la poignée sur la barre verticale : 
```
module cylindre_add(){ //centre le cylindre sur la barre verticale
    rotate([90,0,0])
    translate([0,side*9.5,0])
    difference(){
        cylinder((side/sqrt(2))+1,side/3.8,side/3.8,center = true);
        cylinder(side/sqrt(2),side/3.8,side/3.8,center = true);
}
}
```

Module combiné : 
```
module piece3(){
    cylindre_p();
    cylindre_add();
}
```

![voici le résultat](images/poignee.jpg)

Notre but n'étant pas de rendre cet objet supportable par un être humain, le mécanisme de fixation de la poignéee sur la barre verticale est simplifié et n'est pensé que pour que l'objet soit au complet. 



#### Module barre "pour les pieds"

Cette pièce est censée rentrer par le haut de la barre verticale (sans poignées), nous avons aussi créer des **outils de mesures** afin d'être sur que tout s'emboite, ils seront décris plus bas. 

```
module barre_p(){ // barre pieds
  translate([0,0,-side])
  difference(){ //trou carré
    
    difference(){ //trous cylindres
    
      //barre
    rotate([90,0,90])
    translate([0,-side*3,0])
      cube([side*7,side,side],center=true);
      //trou 1 droite
    rotate([90,0,90])
    translate([side*2,-side*3,0])
      cylinder(side*2,r_trous,side/3,center=true);
      //trou 1 gauche
    rotate([90,0,90])
    translate([-side*2,-side*3,0])
      cylinder(side*2,r_trous,side/3,center=true);
      //trou 2 gauche
    rotate([90,0,90])
    translate([-side*3,-side*3,0])
      cylinder(side*2,r_trous,side/3,center=true);
      //trou 2 droite
    rotate([90,0,90])
    translate([side*3,-side*3,0])
      cylinder(side*2,r_trous,side/3,center=true);
    }
      //trou carré insertion
    translate([0,0,-side*3])
      cube([side*0.72,side*0.72,side],center=true);
  }
}
```

![](images/barre_p.jpg)



#### Outils de mesure 

1. Voici un aperçu de l'outil de mesure qui nous servira à déterminer si il faut rajouter un enlever une infime valeur sur le rayon des cylindres du **compliant mechanism** qui s'emboitera dans la barre horizontale du bas. Nous avons bien évidemment garder les mêmes paramètres de base que la pièce principale afin de savoir moduler le code en fonction de l'emboitement. 5 tailles de "-2 -1 0 +1 +2" par rapport aux valeurs de base du paramètre *"side"*. 
```
module barre_p(){ // 
   
    //barre
  rotate([0,0,90])
  translate([-side*3,-side*3,side*0.5])
    cube([side*5,side*0.8,side],center=true);
    
    
   //embout -2 
  rotate([0,0,90])
  translate([-side*1,-side*3,side])
    cylinder(side,(r_trous)-0.04,(side/3)-0.04,center=true);
  
    //embout -1 
  rotate([0,0,90])
  translate([-side*2,-side*3,side])
      cylinder(side,(r_trous)-0.02,(side/3)-0.02,center=true);
    
    //embout 0 
  rotate([0,0,90])
  translate([-side*3,-side*3,side])
    cylinder(side,(r_trous),(side/3),center=true);

    //embout +1
  rotate([0,0,90])
  translate([-side*4,-side*3,side])
      cylinder(side,(r_trous)+0.02,(side/3)+0.02,center=true);

  //embout +2
  rotate([0,0,90])
  translate([-side*5,-side*3,side])
    cylinder(side,(r_trous)+0.04,(side/3)+0.04,center=true);
}
```

![](images/outil_mesure.jpg)


2. Ce deuxième outil sert à être sûr qu'il n'y ait pas trop de jeu entre la barre pour les pieds et la barre verticale. 

```
module barre_p(){ // barre pieds
  translate([0,0,-side])
  difference(){
      //barre
    rotate([90,0,90])
    translate([0,-side*3,0])
      cube([side*5,side,side],center=true);
        //trou carré insertion -2
    translate([0,-side*2,-side*3])
      cube([side*0.68,side*0.68,side],center=true);
        //trou carré insertion -1
    translate([0,-side,-side*3])
      cube([side*0.70,side*0.70,side],center=true);
        //trou carré insertion 0
    translate([0,0,-side*3])
      cube([side*0.72,side*0.72,side],center=true);
        //trou carré insertion +1
    translate([0,side,-side*3])
      cube([side*0.74,side*0.74,side],center=true);
        //trou carré insertion +2
    translate([0,side*2,-side*3])
      cube([side*0.76,side*0.76,side],center=true);
  }
}
```

![](images/outil2.jpg)



#### Pièce compliant mechanism 

Pour cette pièce, je vous redirige vers la page de mon binome [Martin Loumeau](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/jean-francois.loumeau/fabzero-modules/module02/).



#### Aperçu pièce finale assemblée 

Voici ce à quoi la pièce ressemble sur le logiciel : 

![](images/jumpstick.jpg)

Pour suivre la suite de la confection de notre pièce en 3D, je vous redirige vers [le module 3](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/jules.gerard/fabzero-modules/module03/).



## 2.6 Licences open-source 

Les licences qu'on utilisera pour ce cours sont les *"Creatives Commons licences"*. Ces licences ont pour but de mettre des "conditions" sur l'utilisation et la distribution du travail d'une personne. 

Il en existe de plusieurs types : 

- CC BY : permet aux personnes voulant utiliser, remixer, distribuer ou adapter le travail de quelqu'un d'autre sur n'importe quel format, tant que l'attribution est donnée au créateur. Utilisation commerciale autorisée. 

![](images/CC_BY.jpg) 

- CC BY-SA : les mêmes conditions s'appliquent que pour la licence *CC BY-NC*, l'utilisation commerciale est toujours autorisée mais les adaptations doivent être partagées dans les mêmes conditions. 

![](images/CC_BY_SA.jpg) 

- CC BY-NC : les mêmes conditions s'appliquent mais cette fois ci seules les utilisations non commerciales sont autoriées.

![](images/CC_BY_NC.jpg) 

- CC BY-NC-SA : les mêmes conditions que ci-dessus mais réunies.

![](images/CC_BY_NC_SA.jpg) 

- CC BY-ND : dans ce cas ci il n'y a aucune modification ou dérivé permis.

![](images/CC_BY_ND.jpg) 

- CC BY-NC-ND : les conditions des deux licences séparées réunies. 

![](images/CC_BY_NC_ND.jpg) 

- CC0 ("CC zero") : ceci est une licence spéciale qui donne permission à quiconque de faire ce qu'il veut avec le travail d'une personne. 

![](images/CC_zero.jpg) 


Voici l'exemple de la mienne dans le fichier OpenSCAD de notre jumpstick :

![](images/CC_licence.jpg)
