# 4. 3D printing for repair 

Ce module a pour but de nous faire découvrir comment - avec un des outils qu'on a appris à utiliser (dans ce cas ci l'impression 3D) - réparer un objet devenu obsolète suite à la cassure ou la défaillance d'une des parties de celui-ci.



## 4.1 L'esprit Repair Lab

Dans un monde où l'obsolescence programmée est devenue normalité, des lieux comme le Repair Lab fleurissent de partout. Associé à un souci de durabilité et de responsabilité environnementale, cet état d'esprit est axé sur la résolution de problèmes et la réparation de tous types d'appareils électroniques. On peut aussi parler de son côté communautaire, ces personnes créatives et bricoleurs se retrouvent pour partager leurs connaissances et expertises les uns avec les autres.



### 4.1.1 Boite défaillante

D'entrée de jeu nous avons été confronté à réparer un objet défectueux. Devant nous nous avions une boîte en bois avec une petite ouverture vitrée qui donnait sur une lampe LED, ainsi qu'un interrupteur qui est censé allumer la LED. Nous avions quelques outils à disposition ; un tournevis cruciforme, un plat, un tork et une clé Allen. Nous disposions aussi d'un multimètre digital muni de deux pointes métalliques.

La lampe ne s'allumait pas, il a fallu ouvrir la boîte à l'aide des tournevis adaptés au bon type d'embout des vis. Nous avons donc ouvert la boîte et enlevé un maximum de compartiments pour que tout le matériel à l'intérieur soit accessible. Il y avait une batterie, un intérrupteur, une lampe LED branchée à une "plaque" et le tout était relié par des câbles qui eux mêmes étaient reliés par des dominos. 

La plupart des pannes sont mécaniques ou électroniques, elles sont rarement combinées. À vu d'oeil il n'y avait aucune cassure ou coupure dans le matériel, nous avons donc du utiliser le multimètre pour vérifier l'électronique. 

Il a d'abord fallu **vérifier si la batterie était chargée**, ça se fait sur la **fonction courant continu** (symbolisé par un "V" avec une barre) et à l'échelle appropriée de la pile de 6V, donc on se met à 20V. On se met toujours sur l'échelle supérieure à la tension maximum car sinon l'appareil ne sait pas retourner de valeur. La pile étant une source de tension continue, [le fil rouge doit se brancher sur le + et le fil noir sur le -](https://www.youtube.com/watch?v=iqB18OUzlQU&ab_channel=LeMondedesAvengers). Le multimètre m'a retourné 4.8V, ce qui est suffisant pour allumer la lampe LED. 

Ensuite il a fallu **vérifier si le courant passait** à travers telle ou telle partie du circuit électrique. Cette fois-ci avec le multimètre nous devons nous mettre sur la **fonction testeur de continuité**, si l'appareil retourne un signal sonore c'est que le courant passe. J'ai d'abord testé l'inttérupteur pour être sur qu'il soit fonctionnel et qu'il ferme le circuit électrique. Ensuite j'ai testé chaque fil et chaque domino, à chaque fois ça m'a retourné le signal sonore donc RAS. Il faut faire attention à être rigoureux lors de ces tests car on peut vite se perdre et oublier de tester un fil ou un domino qui pourrait être l'endroit où il y a le problème. Le problème venait de la **lampe LED**, il a suffit de la changer.



### 4.1.2 Les bases de dépannage 

Une certaine méthodologie est applicable lorsqu'un objet casse ou qu'il est défaillant. Il faut tout d'abord se renseigner ou se rappeler sur la façon/le moment où c'est arrivé et aussi savoir si il y avait des signes avant que ça n'arrive. Ensuite on prend l'objet en main, on regarde si il y a des traces suspectes à l'extérieur et on fait très attention à ne pas le brancher avant d'avoir tout vérifié. 

Après ça nous passons à l'utilisation d'outil. Les **outils "d'ouvertures"** comme des tournevis ou des clés à douilles vont servir à ouvrir l'objet sans le casser. Les **outils "de mesure"** vont nous servir à tester certaines choses, il existe de simple tournevis qui s'allume en cas de tension mais nous avons souvent recours aux multimètres analogiques avec des pointes de touches en métal qui savent mesurer tension, courant continu/alternatif, résistance, etc. La liste d'**outils dit "d'intervention"** peut être longue et changera en fonction du problème ciblé en question, on parle de scie, de visseuse, de station de soudage, oscilloscope, etc. Parfois on peut utiliser certains produits en fonction aussi du problème en question, par exemple de l'huile, du wd40, des colles diverses, etc. 

Lorsqu'une panne est électrique, on va désosser l'objet et avoir recours à un ohmmètre. Il faudra suivre le chemin qu'est censé prendre l'électricité et tester chaque morceaux du circuit. Si l'ohmmètre retourne une valeur proche de zéro c'est que le courant passe, si il retourne l'infini c'est que rien ne passe. Logiquement à un moment on arrive au composant défectueux. Il existe plusieurs composants de base dont voici une photo suivi d'une liste auxquels nous avons été introduit : 

![](images/composants.jpg)

- batterie
- fusible 
- condensateur 
- résistance



## 4.2 La réparation de l'objet

Nous avons choisi comme objet à réparer une lampe de vélo Décathlon dont l'accroche avait laché, ce qui empêche son utilisation. 

Nous avons d'abord essayé de désassembler la lampe mais impossible de l'ouvrir, les différentes parties étaient thermiquement collées. Nous voulions, à la base, tout simplement réimprimer l'accroche mais nous avons du nous adapter à cela en repensant nos solutions de réparation. Nous sommes donc partis sur une sorte de coque avec le même type d'accroche qu'il y avait à la base. Je possédais une lampe de la même marque et nous nous en sommes servis pour reproduire l'accroche de la même manière. 

Nous avons d'abord du enlever le reste de l'accroche cassée et ensuite poncer l'arrière afin d'applatir au maximum cette partie de la lampe. J'ai utilisé une scie puis du papier ponce pour faire cela. Pendant ce temps là, mon binôme a commencé à designer notre objet sur OpenScad. 

![](images/scie.jpg)

![](images/ponce.jpg)

Voici le résultat :

![](images/plate.jpg)

![](images/plate_face.jpg)

Je vous redirige vers la page de [Martin Loumeau](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/jean-francois.loumeau/fabzero-modules/module04/) pour voir l'acheminement de modélisation qui a donné ceci : 

![](images/proto1.jpg)

![](images/proto2.jpg)


Lors de l'impression à cause de la chaleur et du fait que je n'avais pas rajouter de support, une petite partie ne s'est pas bien imprimé mais ça n'a pas empêcher la pièce d'être fonctionnelle! 

![](images/proto_comp.jpg)

Notre prorotype a directement donné un objet usuel et qui répondait au problème inital. Nous avons laissé un espace au niveau du chargeur afin de ne pas devoir l'enlever de son socle. L'espace pour la lampe est parfaitement à la bonne taille et empêche qu'elle ne se défasse toute seule. L'accroche a la bonne résistance et a l'air assez solide pour s'attacher sur un vélo, elle permet aussi de l'accrocher à un tissu ou à son sac à dos sans soucis : 

![](images/proto_accroche.jpg)

